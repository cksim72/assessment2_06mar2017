(function () {
    'use strict';
    angular
        .module("GMS")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "$state", "$stateParams", "GrocerySvc"];

    function EditCtrl($filter, $state, $stateParams, GrocerySvc) {

        var vm = this;

        vm.brand = "";
        vm.product = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        vm.initDetails = initDetails;
        // vm.search = search;
        vm.toggleEditor = toggleEditor;
        // vm.updateDeptName = updateDeptName;
        vm.updateProduct = updateProduct;
        vm.goSearch = goSearch;


        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();

        if($stateParams) {
            vm.product = JSON.parse($stateParams.product);
            console.log("vm.product " + JSON.stringify(vm.product));
            console.log("vm.product.name " + vm.product.name);
        }

        function initDetails() {
            console.log("-- edit.controller.js > initDetails()");
            vm.product.brand = "";
            vm.product.name = "";
            vm.product.upc12 = "";

            // vm.showDetails = false;
            // vm.isEditorOn = false;
            
        }

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

        function goSearch()
        {
            $state.go("search");
        }

        // Saves edited product info
        function updateProduct() {
            console.log("Product Info", vm.product);
            GrocerySvc
                .updateProduct(vm.product)
                .then(function (result) {
                    console.log("Result" + JSON.stringify(result.data));
                    goSearch();
                })
                .catch(function (err) {
                    console.log("update Product error: \n" + JSON.stringify(err));
                });
         }
    }
})();
