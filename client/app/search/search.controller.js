(function () {
    angular
        .module("GMS")
        .controller("SearchDBCtrl", SearchDBCtrl);

    SearchDBCtrl.$inject = ['$state', 'GrocerySvc'];

    function SearchDBCtrl($state, GrocerySvc) {
        var vm = this;

        vm.searchStringName = '';
        vm.searchStringBrand = '';
        vm.searchString = '';
        vm.result = null;
        vm.showSearch = false;
        vm.groceryList=[];
        vm.product = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.

        vm.goEdit = goEdit;
        vm.searchByName = searchByName;
        vm.searchByBrand = searchByBrand;

        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        // init is a private function (i.e., not exposed)
        init();

        // Function declaration and definition -------------------------------------------------------------------------
        function goEdit(index){
            // console.log("index:" + index);
            vm.product=vm.searchList[index];
            console.log("product: " + JSON.stringify(vm.product));
            $state.go("editWithParam",{product: JSON.stringify(vm.product)});
        }

        // The init function initializes view
        function init() {
            // GrocerySvc
            //     .retrieveDBAll()
            //     .then(function (results) {
            //         vm.groceryList = results.data;
            //         console.log("Grocery List : " + JSON.stringify(vm.groceryList));
            //     })
            //     .catch(function (err) {
            //         console.log("error " + err);
            //     });

        }

        function searchByID() {
            // vm.showSearch = false;
            GrocerySvc
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .retrieveDBByID(vm.productID)
                .then(function (results) {
                    vm.searchList = results.data;
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
        }

        // Search by Brand
        function searchByBrand() {
            // vm.showSearch = false;
            console.log(vm.searchStringBrand);
            GrocerySvc
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .searchDBbyBrand(vm.searchStringBrand)
                .then(function (results) {
                    vm.searchList = results.data;
                    console.log("Brand - Search List:" + JSON.stringify(vm.searchList));
                    vm.showSearch=true;
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
        }

        // Search by Name
        function searchByName() {
            // vm.showSearch = false;
            GrocerySvc
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .searchDBbyName(vm.searchStringName)
                .then(function (results) {
                    vm.searchList = results.data;
                    console.log("Name - Search List:" + JSON.stringify(vm.searchList));
                    vm.showSearch=true;
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
        }

        // Search by searchString
        function searchDB() {
            // vm.showSearch = false;
            GroceryService
                // we pass contents of vm.searchString to service so that we can search the DB for this string
                .searchDB(vm.searchString)
                .then(function (results) {

                    vm.searchList = results.data;
                })
                .catch(function (err) {
                    // We console.log the error. For a more graceful way of handling the error, see
                    // register.controller.js
                    console.log("error " + err);
                });
        }
    }
})();