(function () {
    angular
        .module("GMS", [
            "ngMessages" 
            , "ngAnimate" 
            , "ui.router" 
        ]);
})();