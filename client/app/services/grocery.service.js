
// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("GMS")
        .service("GrocerySvc", GrocerySvc);

    GrocerySvc.$inject = ['$http'];


    function GrocerySvc($http) {

    var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.searchDB = searchDB;
        service.retrieveDBbyID = retrieveDBbyID;
        service.retrieveDBAll = retrieveDBAll;
        service.updateProduct = updateProduct;
        service.searchDBbyName = searchDBbyName;
        service.searchDBbyBrand = searchDBbyName;

        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------

        // Search based on search String
        function searchDB(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/search'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // retrieveDBbyID retrieves product information
        function retrieveDBbyID(productID) {
            return $http({
                method: 'GET'
                , url: "api/search/id" + productID
            });
        }

        // retrieveDBbyID retrieves product information
        function searchDBbyBrand(brand) {
            console.log("SearchbyBrand activated");
            return $http({
                method: 'GET'
                , url: "api/search/brand"
                , params: {
                    'searchString': brand
                    }
            });
        }

        // retrieveDBbyID retrieves product information
        function searchDBbyName(name) {
            return $http({
                method: 'GET'
                , url: "api/search/name"
                , params: {
                    'searchString': name
                }
            });
        }

        // retrieveDBAll - Retrieve all info from database
        function retrieveDBAll() {
            return $http({
                method: 'GET'
                , url: 'api/search'
                , params: {
                    'searchString': ''
                }
            });
        }

        // updateProduct Info
        function updateProduct(productInfo) {
            return $http({
                method: 'PUT'
                , url: 'api/product/update'
                , data: {
                    id: productInfo.id,
                    brand: productInfo.brand,
                    name: productInfo.name,
                    upc12: productInfo.upc12
                }
            });
        }
    }
})();