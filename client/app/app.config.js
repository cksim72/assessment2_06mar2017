// Defines client-side routing
(function () {
    angular
        .module("GMS")
        .config(groceryRouteConfig)
    groceryRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function groceryRouteConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('search', {
                url: '/search',
                templateUrl: './app/search/search.html',
                controller: 'SearchDBCtrl',
                controllerAs: 'ctrl'
            })
            .state('editWithParam', {
                url: '/editWithParam/:product',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/search");
    }
})();