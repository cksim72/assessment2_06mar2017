// DEPENDENCIES ------------------------------------------------------------------------------------------------------
// Loads express module and assigns it to a var called express
var express = require("express");

// Loads path to access helper functions for working with files and directory paths
var path = require("path");

// Loads bodyParser to populate and parse the body property of the request object
var bodyParser = require("body-parser");

// Loads sequelize ORM
var Sequelize = require("sequelize");

// CONSTANTS ---------------------------------------------------------------------------------------------------------
// Defines server port.
// Value of NODE_PORT is taken from the user environment if defined; port 3000 is used otherwise.
const NODE_PORT = process.env.PORT || 5000;

// Defines paths
// __dirname is a global that holds the directory name of the current module
const CLIENT_FOLDER = path.join(__dirname + '/../client');  // CLIENT FOLDER is the public directory
const MSG_FOLDER = path.join(CLIENT_FOLDER + '/assets/messages');

// Defines MySQL configuration
const MYSQL_USERNAME = 'root';
const MYSQL_PASSWORD = 'sim#!1972';

// OTHER VARS ---------------------------------------------------------------------------------------------------------
// Creates an instance of express called app
var app = express(); 

//Database named "grocery_list"
var sequelize = new Sequelize(
    'grocery_list',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',         // default port    : 3306
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }
    }
);

// Loads model for department table
var Grocery = require('./models/grocery')(sequelize, Sequelize);

// Department.hasMany(Manager, {foreignKey: 'dept_no'});
// Manager.hasOne(Employee, {foreignKey: 'emp_no'});

// MIDDLEWARES --------------------------------------------------------------------------------------------------------
app.use(express.static(CLIENT_FOLDER));

app.use(bodyParser.json());


// ROUTE HANDLERS -----------------------------------------------------------------------------------------------------

// SEARCH Endpoint for search comparing either brand or product name
// Endpoint: /api/search
app.get("/api/search", function (req, res) {
    Grocery
    // findAll asks sequelize to search
        .findAll({
            where: {
                $or: [
                    {name: {$like: "%" + req.query.searchString + "%"}},
                    {brand: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
        })
        .then(function (groceryList) {
            res
                .status(200)
                .json(groceryList);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});

app.get("/api/search/brand", function (req, res) {
    Grocery
    // findAll asks sequelize to search
        .findAll({
            where: {brand: {$like: "%" + req.query.searchString + "%"}}
        })
        .then(function (groceryList) {
            res
                .status(200)
                .json(groceryList);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});

app.get("/api/search/name", function (req, res) {
    Grocery
    // findAll asks sequelize to search
        .findAll({
            where: {name: {$like: "%" + req.query.searchString + "%"}}
        })
        .then(function (groceryList) {
            res
                .status(200)
                .json(groceryList);
        })
        .catch(function (err) {
            res
                .status(500)
                .json(err);
        });
});


// -- Updates Product info
app.put('/api/product/update', function (req, res) {
    var where = {};
    where.id = req.body.id;

    console.log("Update Data: " + JSON.stringify(req.body));

    // Update details
    Grocery
        .update(
            {
                id: req.body.id,
                brand: req.body.brand,
                name: req.body.name,
                upc12: req.body.upc12
            },
            {where: where}
        )
        .then(function (response) {
            console.log("-- Product Update then(): \n"
                + JSON.stringify(response));
            res.json(response);
        })
        .catch(function (err) {
            console.log("-- Product Update catch(): \n"
                + JSON.stringify(err));
        });
});

app.get("/api/product/:productID", function (req, res) {
    console.log
    var where = {};

    if (req.params.productID) {
        where.id = req.params.productID
    }

    console.log("where " + where);
    
    Grocery
        .findOne({
            where: where
        })
        .then(function (result) {
            console.log("Update Product success \n " + JSON.stringify(result));
            res.json(result);
        })
        // this .catch() handles erroneous findAll operation
        .catch(function (err) {
            console.log("Update Product catch() \n " + JSON.stringify(departments));
            res
                .status(500)
                .json({error: true});
        });

});


// ERROR HANDLING ----------------------------------------------------------------------------------------------------
// Handles 404. In Express, 404 responses are not the result of an error,
// so the error-handler middleware will not capture them.
// To handle a 404 response, add a middleware function at the very bottom of the stack
// (below all other path handlers)
app.use(function (req, res) {
    res.status(404).sendFile(path.join(MSG_FOLDER + "/404.html"));
});

// Error handler: server error
app.use(function (err, req, res, next) {
    res.status(501).sendFile(path.join(MSG_FOLDER + '/501.html'));
});


// SERVER / PORT SETUP ------------------------------------------------------------------------------------------------
// Server starts and listens on NODE_PORT
app.listen(NODE_PORT, function () {
    console.log("Server running at PORT: " + NODE_PORT);
});